//
//  ViewController.swift
//  Шифратор сообщений
//
//  Created by Алексей on 29.10.2018.
//  Copyright © 2018 Алексей. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var label: UILabel!
    
    let core = Core()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func buttonPressedEncrypt() {
        let asciiArray = textView.text!.compactMap {(
            $0.unicodeScalars.first?.value
        )}
            
        /*print("\(asciiArray)")
        print("\(core.encryptMass(asciiArray))")*/
        var message: String = ""
        let encryptedMass = core.encryptMass(asciiArray)
        for item in encryptedMass {
            message = message + "\(String(item)) "
        }
        label.text = message
    }
    
    @IBAction func sendMessage() {
        let activityViewController = UIActivityViewController(activityItems: [label.text as Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func clear() {
        label.text = ""
        textView.text = ""
    }
    
}

