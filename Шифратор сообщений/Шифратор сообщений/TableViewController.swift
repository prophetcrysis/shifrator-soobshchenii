//
//  TableViewController.swift
//  Шифратор сообщений
//
//  Created by Алексей on 04.02.2019.
//  Copyright © 2019 Алексей. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    /*let data: [String] = ["row 1",
                          "row 2",
                          "row 3",
                          "row 4"]*/
    
    let sec1 = "sec1"
    let sec2 = "sec2"
    /*let section = ["test1", "test2", "test3"]
    let items = [["test1", "test2", "test"]]*/
    
    lazy var sectionTitles: [String] = {
        return [sec1, sec2]
    }()
    
    lazy var data: [String:[String]] = {
        return [sec1: ["row 1",
                       "row 2",
                       "row 3",
                       "row 4"],
                sec2: ["row 1",
                       "row 2",
                       "row 3",
                       "row 4"]]
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return data.keys.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return data[sectionTitles[section]]!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if let TableViewCell = cell as? TableViewCell {
            let sec = sectionTitles[indexPath.section]
            let arr = data[sec]
            TableViewCell.label.text = arr![indexPath.item]
        }
        return cell
    }
    
//    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60))
//        let lable = UILabel(frame: view.frame)
//        lable.textAlignment = .center
//        lable.text = ""
//        return view
//    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if let TableViewCell = cell as? TableViewCell {
            TableViewCell.label.text = "selected"
        }
    }

}
