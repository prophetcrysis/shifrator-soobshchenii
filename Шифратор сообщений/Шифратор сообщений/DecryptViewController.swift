//
//  DecryptViewController.swift
//  Шифратор сообщений
//
//  Created by Алексей on 07.02.2019.
//  Copyright © 2019 Алексей. All rights reserved.
//

import UIKit

class DecryptViewController: UIViewController {
    
    let core = Core()
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var label: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buttonPressedDecrypt() {
        let encryptedMass = textView.text.components(separatedBy: " ")
        let encryptedMassInt = encryptedMass.compactMap ({
            Int($0)
        })
        let decryptedMass = core.decryptMass(encryptedMassInt)
        var message: String = ""
        for item in decryptedMass {
            message = message + "\(String(UnicodeScalar(item)!))"
        }
        label.text = message
    }
    
    @IBAction func clear() {
        label.text = ""
        textView.text = ""
    }
    
}
