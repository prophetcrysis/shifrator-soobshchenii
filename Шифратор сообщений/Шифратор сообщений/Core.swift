//
//  Core.swift
//  Шифратор сообщений
//
//  Created by Алексей on 29.10.2018.
//  Copyright © 2018 Алексей. All rights reserved.
//

import Foundation

class Core {
    
    func encryptMass(_ inputValue: [UInt32]) -> [Int] {
        var exitValue = [Int]()
        for item in inputValue {
            exitValue.append(encrypt(item))
        }
        return exitValue
    }
    
    func decryptMass(_ inputValue: [Int]) -> [Int] {
        var exitValue = [Int]()
        for item in inputValue {
            exitValue.append(decrypt(String(item)))
        }
        return exitValue
    }
    
    func encrypt(_ inputValue: UInt32) -> Int {
        var divValue: Int = Int(inputValue)
        var modValue: Int = 0
        var j: Int = -1
        var binaryCode = [Int]()
        var exitValue: String = ""
        while divValue != 0 {
            modValue = divValue % 2
            divValue = divValue / 2
            binaryCode.append(modValue)
            j += 1
        }
        while j >= 0 {
            exitValue += String(binaryCode[j])
            j -= 1
        }
        return Int(exitValue)!
    }
    
    func decrypt(_ inputValue: String) -> Int{
        var umn: Int = 1
        var zeroOrOne: Int = 0
        var number = [Int]()
        var numbers = [Int]()
        var exitValue: Int = 0
        /*for i in inputValue.characters {
            let someString = String(i)
            if let someInt = Int(someString) {
                numbers += [someInt]
            }
        }*/
        numbers = inputValue.compactMap { Int(String($0))! }
        if numbers[numbers.count - 1] == 1 {
            zeroOrOne = 1
        }
        numbers.removeLast()
        for _ in 1 ... numbers.count {
            umn *= 2
        }
        
        for i in 1 ... numbers.count {
            number.append(numbers[i - 1] * umn)
            umn = umn / 2
        }
        for i in 1 ... number.count {
            exitValue += (number[i - 1])
        }
        exitValue += zeroOrOne
        return exitValue
    }
}
